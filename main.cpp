#include <exem.h>
#include <exem.cpp>

int main()
{
    srand(time(NULL));
    Lines L(10);

    L.random();
    L.show();
    std::cout<< std::endl;

    L.sort();
    L.show();

    return 0;
}
