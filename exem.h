#ifndef EXEM_H
#define EXEM_H

#include <iostream>
#include <vector>

class Lines{
    struct Line{
        size_t Begin;
        size_t End;

        Line();
    };
    size_t Amount;

    std::vector <Line> lines;

public:

    Lines(size_t amount);

    void show();

    void random();

    bool compare(size_t Index1, size_t Index2);

    size_t counter_max();

    void sort();
};


#endif // EXEM_H
