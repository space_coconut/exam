#include <exem.h>
//#include <time.h>
#include <ctime>



Lines::Line::Line(){
    Begin = 0;
    End = 0;
}

Lines::Lines(size_t amount){
    Amount = amount;
    lines.resize(Amount, Line());
}

void Lines::show(){
    for(size_t i=0; i<Amount; i++)
        std::cout<< lines[i].Begin << "-" << lines[i].End << std::endl;
}

void Lines::random(){
    for(size_t i = 0; i < Amount; i++){
        lines[i].Begin = rand()%10;
        lines[i].End = rand()%10;
        if(lines[i].Begin>lines[i].End){
            size_t O = lines[i].Begin;
            lines[i].Begin = lines[i].End;
            lines[i].End = O;
        }
    }
}

bool Lines::compare(size_t Index1, size_t Index2){
    if(lines[Index1].End <= lines[Index2].Begin || lines[Index1].Begin >= lines[Index2].End){
        return false;
    }
    else return true;
}


size_t Lines::counter_max(){
    size_t Count = 0;
    for(size_t i = 0; i < Amount; i++){
        if(lines[0].End <= lines[0].Begin || lines[0].Begin >= lines[0].End)
            Count++; //+1, если не пересекаются
    }
}

void Lines::sort(){
    size_t temp;
    size_t temp2;
    for (size_t i = 0; i < Amount - 1; i++) {
        for (size_t j = 0; j < Amount - i - 1; j++) {
            if (lines[j].Begin > lines[j + 1].Begin) {
                temp = lines[j].Begin;
                temp2 = lines[j].End;
                lines[j].Begin = lines[j + 1].Begin;
                lines[j].End = lines[j + 1].End;
                lines[j + 1].Begin = temp;
                lines[j + 1].End = temp2;
            }
        }
    }

    for (size_t k = 0; k < Amount; k++){
        size_t begin;
        size_t end;
        size_t count = 0;
        if(lines[k].Begin == k){
            count++;
            if(count == 1)
                begin = k;
        }
    }
}
